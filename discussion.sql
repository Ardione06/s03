-- MySQL CRUD Operations

-- Creating new records

-- Syntax1
-- INSERT INTO <table_name> (<column_name>)
-- VALUES (<value>);

INSERT INTO artists(name) VALUES("Rivermaya");

-- INSERT INTO <table_name> (<column_name>, ....)
-- VALUES (<value>, ....);


-- Mini-activity #1
-- Insert a new record in artists table with value 'Psy'
INSERT INTO artists(name) VALUES("Psy");

-- Mini-activity #2
INSERT INTO albums(album_name, year, artist_id)
VALUES("Psy 6", "2021-1-1", 2), 
("Trip", "1996-1-1", 1);

INSERT INTO albums(album_name, year, artist_id) 
VALUES("Psy 6", "2021-1-1", 2);

INSERT INTO albums(album_name, year, artist_id) 
VALUES("Trip", "1996-1-1", 1);

-- Mini-activity #3
-- Insert new records in 'songs' table with the ff values;

INSERT INTO songs(title, length, genre, album_id)
VALUES 	("Gangnam Style", 253, "K-pop", 7),
		("Kundiman", 234, "OPM", 8),
		("Kisapmata", 319, "OPM", 8);


-- Retrieving existing records

SELECT * FROM artists;

SELECT id, name FROM artists WHERE name="Rivermaya" AND id = 1;


SELECT title, genre FROM songs WHERE id = 1;

SELECT genre FROM songs;

SELECT title, genre 
FROM songs 
WHERE id = 2;


SELECT album_name
FROM albums 
WHERE artist_id = 1;

SELECT title, length
FROM songs
WHERE album_id = 8
AND length > 240;


SELECT title, length
FROM songs
WHERE album_id = 8
AND length > 240;

-- Updating records
-- Update <table>
-- SET <column> = <new_value>
-- WHERE <condition>;

UPDATE songs
SET length = 240
WHERE id = 2;

UPDATE songs
SET length = 240
WHERE title = 'Kundiman';


-- Deleting records
-- DELETE FROM <table>
-- WHERE <condition>
-- AND|OR <condition>

DELETE FROM songs 
WHERE genre = 'OPM'
AND length >= 200;

